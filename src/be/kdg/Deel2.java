package be.kdg;

import java.util.Scanner;

public class Deel2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String answer = "";
        int day = 0;
        int calmBeat = 0;
        int maxBeat = 0;

        System.out.print("Wat is uw rusthartslag? ");
        calmBeat = keyboard.nextInt();
        System.out.print("Wat is uw maximum hartslag? ");
        maxBeat = keyboard.nextInt();

        int one = (int)((maxBeat - calmBeat) * 0.5 + calmBeat);
        int two = (int)((maxBeat - calmBeat) * 0.6 + calmBeat);
        int three = (int)((maxBeat - calmBeat) * 0.7 + calmBeat);
        int four = (int)((maxBeat - calmBeat) * 0.8 + calmBeat);
        int five = (int)((maxBeat - calmBeat) * 0.9 + calmBeat);

        while (!(answer.toUpperCase().equals("B") || answer.toUpperCase().equals("G"))){
            System.out.print("Bent u een beginner (B) of gevorderde (G)? ");
            answer = keyboard.next();
        }
        System.out.print("De hoeveelste dag voert u dit schema uit? ");
        day = keyboard.nextInt();

        if (answer.toUpperCase().equals("B")){
            System.out.printf("Beginner dag %d: \n", day);
            System.out.printf("1. loop tss %d en %d : **********", three, five);
            for (int i = 0; i < day; i++) {
                System.out.print("*");
            }
            System.out.println();
            System.out.printf("2. Loop tss %d en %d : --------------------", five, maxBeat);
            if (day < 10) {
                for (int i = 0; i < day; i++) {
                    System.out.print("-");
                }
            }
            if (day >= 10){
                for (int i = 0; i < 10; i++) {
                    System.out.print("-");
                }
            }
            System.out.println();
            System.out.printf("3. Wandel tot maximale harstslag %d\n", three);
            System.out.printf("4. Wandel tss %d en %d : ----------------------------------------", one, three);
            for (int i = 0; i < day; i++) {
                System.out.print("-");
            }
            System.out.println();
            System.out.printf("5. Wandel tss %d en %d : **********", two, three);
            for (int i = 0; i < day; i++) {
                System.out.print("*");
            }
            System.out.println();
        }

        if (answer.toUpperCase().equals("G")){
            System.out.printf("Gevorderde dag %d: \n", day);
            System.out.printf("1. loop tss %d en %d : ********************", three, five);
            for (int i = 0; i < day; i++) {
                System.out.print("*");
            }
            System.out.println();
            System.out.printf("2. Loop tss %d en %d : ------------------------------\n", five, maxBeat);
            System.out.printf("3. Wandel tot maximale harstslag %d\n", three);
            System.out.printf("4. Wandel tss %d en %d : ----------------------------------------", one, three);
            for (int i = 0; i < day; i++) {
                System.out.print("-");
            }
            System.out.println();
            System.out.printf("5. Wandel tss %d en %d : **********", two, four);
            for (int i = 0; i < day; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
