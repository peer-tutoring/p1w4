package be.kdg;

import java.util.Scanner;

public class Deel1 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int maxHeart = 0;
        int calmHeart = 0;
        int hrr = 0;
        char answer = 'o';
        int zone;
        double lowerLimit = 0;
        double upperLimit = 0;

        System.out.print("Ken je je HRR? ");
        answer = keyboard.next().toLowerCase().charAt(0);
        if (answer == 'j'){
            System.out.print("Wat is je rusthartslag?");
            calmHeart = keyboard.nextInt();
            System.out.print("Wat is je maximum hartslag?");
            maxHeart = keyboard.nextInt();
            System.out.print("Wat is je HRR? ");
            hrr = keyboard.nextInt();
        }
        if (answer == 'n'){
            System.out.println("Ga even rustig zitten en adem 10x diep in en uit..."
                    + "\n  Indien je klaar bent typ je ok");

            while(!keyboard.nextLine().toLowerCase().equals("ok")) {
                System.out.println("Typ 'ok' als je klaar bent...");
            }

            countDown();
            System.out.print("Geef het aantal slagen in:");
            calmHeart = keyboard.nextInt() * 4;
            System.out.println("===========================================");
            System.out.println("Gelieve volgende instructies uit te voeren:\n\n" +
                    "Interval 1: Ren 30 seconden op volle snelheid.\n" +
                    "Herstelperiode 1: Rust 60 seconden.\n" +
                    "Interval 2: Ren 30 seconden op volle snelheid.\n" +
                    "Herstelperiode 2: Rust 60 seconden.\n" +
                    "Interval 3: Ren 60 seconden op volle snelheid.\n");
            System.out.println("===========================================");
            System.out.println("Indien je klaar bent typ ok");
            while(!keyboard.nextLine().toLowerCase().equals("ok")) {
                System.out.println("Typ 'ok' als je klaar bent...");
            }

            countDown();
            System.out.print("Geef het aantal slagen in: ");
            maxHeart = keyboard.nextInt() * 4;

            hrr = maxHeart - calmHeart;
        }

        System.out.printf("Uw HRR: %d", hrr);
        System.out.print("\nIn welke zone wilt u trainen (1-5)? ");
        zone = keyboard.nextInt();

        switch (zone){
            case 1:
                lowerLimit = (maxHeart - calmHeart) * 0.5 + calmHeart;
                upperLimit = (maxHeart - calmHeart) * 0.6 + calmHeart;
                break;
            case 2:
                lowerLimit = (maxHeart - calmHeart) * 0.6 + calmHeart;
                upperLimit = (maxHeart - calmHeart) * 0.7 + calmHeart;
                break;
            case 3:
                lowerLimit = (maxHeart - calmHeart) * 0.7 + calmHeart;
                upperLimit = (maxHeart - calmHeart) * 0.8 + calmHeart;
                break;
            case 4:
                lowerLimit = (maxHeart - calmHeart) * 0.8 + calmHeart;
                upperLimit = (maxHeart - calmHeart) * 0.9 + calmHeart;
                break;
            case 5:
                lowerLimit = (maxHeart - calmHeart) * 0.9 + calmHeart;
                upperLimit = (maxHeart - calmHeart) * 1.0 + calmHeart;
                break;
        }

        System.out.printf("Uw ideale hartslag voor deze oefening is tussen %1.0f en %1.0f", lowerLimit, upperLimit);
    }

    public static void countDown(){
        System.out.println("start");
        for (int i = 15; i > 0; i--) {
            System.out.printf("%d - ", i);
            try{
                Thread.sleep(1000);
            }catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        System.out.println("stop");
    }
}



